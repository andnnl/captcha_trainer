import time
import muggle_ocr
import os

# sdk = muggle_ocr.SDK(model_type=muggle_ocr.ModelType.OCR)
# sdk = muggle_ocr.SDK(model_type=muggle_ocr.ModelType.Captcha)

yaml_path = r'./out/model/vc_model.yaml'
sdk = muggle_ocr.SDK(model_type=muggle_ocr.ModelType.Captcha,conf_path=yaml_path)
# url = 'http://www.xxx/image/11.jpg'
# response = requests.get(url, verify=False)
# text = self.sdk.predict(image_bytes=response.content)

root_dir = r"./imgs"
for i in os.listdir(root_dir):
    n = os.path.join(root_dir, i)
    print(n)
    with open(n, "rb") as f:
        b = f.read()
    st = time.time()
    text = sdk.predict(image_bytes=b)
    print(i, text, time.time() - st)